# Alertmanager to ESS Notify


Python web server to forward (and curate) messages to the ESS notify service.


alertmanager-to-ess-notify is built with [FastAPI].


## Configuration

The service is configured via the `settings.yaml` file.
The path of the file can be redefined via the `APP_CONFIG_FILE` environmental variable.

An example configuration can be found in the `tests/settings.yaml` file.
The receiver name _must_ match the name used in the alertmanager configuration (`ESSnotify` in the example config file).

The strategy parameter will change the way this bridge is formatting the notification sent to the ESS Notify service. At the moment, only two strategies are available, `one_to_one` and `emx`. The former will create one notification per alarm, the latter will try to extract and format the notification by aggregation the labels defined in the EMX notification rule.


## Development

Python >= 3.8 is required.
Create a virtual environment and install the requirements:

```bash
python3 -m venv .venv
source .venv/bin/activate
pip install -r requirements.txt
pip install -e .[tests]
```

### Testing
[pytest] is used for testing.
Create and activate a virtual environment as detailed above.
```bash
pytest -v tests
```
A `docker-compose` file is available to create a small testing environment. Be aware: when using this setup the actual calls to the notify server are **NOT** mocked.

## Deploy
The application can be deployed via the provided `Dockerfile` or by creating a virtual environment (via `conda` or `venv`)
