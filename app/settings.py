import os
import yaml
from pathlib import Path
from typing import Dict, Any

from pydantic import BaseSettings, BaseModel
from .strategies import StartegyEnum


def yaml_config_settings_source(settings: BaseSettings) -> Dict[str, Any]:
    """
    A simple settings source that loads variables from a YAML file
    at the project's root.
    """
    path = os.environ.get("APP_CONFIG_FILE", "settings.yaml")
    encoding = settings.__config__.env_file_encoding
    return yaml.safe_load(Path(path).read_text(encoding))


class ServiceIDMap(BaseModel):
    service_id: str
    strategy: StartegyEnum


class Settings(BaseSettings):
    notify_url: str
    receivers: Dict[str, ServiceIDMap]

    class Config:
        env_file_encoding = "utf-8"

        @classmethod
        def customise_sources(
            cls,
            init_settings,
            env_settings,
            file_secret_settings,
        ):
            return (
                init_settings,
                yaml_config_settings_source,
                env_settings,
                file_secret_settings,
            )
