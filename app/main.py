import logging
from functools import lru_cache
from fastapi import FastAPI, Depends
from .alertmanagerschemas import AlertGroup
from fastapi.logger import logger
from .essnotify import send_notification
from .settings import Settings
from .strategies import STRATEGIES

# The following logging setup assumes the app is run with gunicorn
gunicorn_error_logger = logging.getLogger("gunicorn.error")
uvicorn_access_logger = logging.getLogger("uvicorn.access")
uvicorn_access_logger.handlers = gunicorn_error_logger.handlers
logger.handlers = gunicorn_error_logger.handlers
logger.setLevel(gunicorn_error_logger.level)


@lru_cache()
def get_settings():
    return Settings()


app = FastAPI()


@app.post("/webhook")
def forward_notification(
    alert_group: AlertGroup, settings: Settings = Depends(get_settings)
):
    logger.info("AlertGroup received")
    logger.debug("Received: -> %s", alert_group.dict())
    if alert_group.receiver not in settings.receivers:
        logger.error("Receiver %s not defined in config", alert_group.receiver)
        return False
    strategy = settings.receivers[alert_group.receiver].strategy.name
    for notification in STRATEGIES[strategy](alert_group=alert_group):
        send_notification(
            notification=notification,
            service_id=settings.receivers[alert_group.receiver].service_id,
            notify_baseurl=settings.notify_url,
        )
    return True


@app.get("/health")
def heatlh():
    return True
