from datetime import datetime
from pydantic import BaseModel, Field
from typing import Dict, List


class Alert(BaseModel):
    fingerprint: str
    status: str
    annotations: Dict[str, str]
    labels: Dict[str, str]
    starts_at: datetime = Field(alias="startsAt")
    ends_at: datetime = Field(alias="endsAt")
    generator_url: str = Field(alias="generatorURL")

    class Config:
        extra = "allow"
        allow_population_by_field_name = True


class AlertGroup(BaseModel):
    alerts: List[Alert]
    receiver: str
    status: str
    version: str
    group_key: str = Field(alias="groupKey")
    truncated_alerts: int = Field(alias="truncatedAlerts", default=0)
    group_labels: Dict[str, str] = Field(alias="groupLabels")
    common_annotations: Dict[str, str] = Field(alias="commonAnnotations")
    common_labels: Dict[str, str] = Field(alias="commonLabels")

    class Config:
        extra = "allow"
        allow_population_by_field_name = True
