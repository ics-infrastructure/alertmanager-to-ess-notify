from app import essnotify


def test_send_notification(requests_mock):
    adapter = requests_mock.post(
        "https://notify.esss.lu.se/api/v2/services/fake_service_id/notifications"
    )
    notification = essnotify.Notification(
        title="title1", message="ciao", url="http://i.do.not.exist"
    )
    essnotify.send_notification(
        service_id="fake_service_id",
        notify_baseurl="https://notify.esss.lu.se/api/v2/services",
        notification=notification,
    )
    assert adapter.call_count == 1
    response = {"subtitle": "ciao", "title": "title1", "url": "http://i.do.not.exist"}
    assert adapter.last_request.json() == response
    notification2 = essnotify.Notification(title="title2", message="ciaone")
    essnotify.send_notification(
        service_id="fake_service_id",
        notify_baseurl="https://notify.esss.lu.se/api/v2/services",
        notification=notification2,
    )
    assert adapter.call_count == 2
    response2 = {"subtitle": "ciaone", "title": "title2"}
    assert adapter.last_request.json() == response2
