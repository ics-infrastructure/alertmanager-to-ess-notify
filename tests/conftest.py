import pytest
from fastapi.testclient import TestClient
from typing import Generator, Dict
from app.main import app


@pytest.fixture
def client() -> Generator:
    with TestClient(app) as c:
        yield c


@pytest.fixture
def fake_alert_group1() -> Dict:
    test1 = {
        "alerts": [
            {
                "fingerprint": "2a3486e2f4bd245d",
                "status": "firing",
                "annotations": {
                    "description": "Service cadvisor:8080 is unavailable.",
                    "runbook": "http://wiki.alerta.io/RunBook/{app}/Event/{alertname}",
                    "value": "DOWN (0)",
                    "summary": "Something is wrong",
                },
                "labels": {
                    "alertname": "service_down",
                    "correlate": "service_up,service_down",
                    "dc": "eu-west-1",
                    "instance": "cadvisor:8080",
                    "job": "cadvisor",
                    "monitor": "codelab",
                    "region": "EU",
                    "severity": "major",
                    "csentry_group_mtca_amc": "1",
                    "csentry_vlan_id": "3880",
                    "csentry_device_type": "MTCA-AMC",
                    "csentry_group_labnetworks": "1",
                    "environment": "cslab.esss.lu.se",
                    "service": "DC:OS",
                    "csentry_group_physicalmachine": "1",
                },
                "starts_at": "2021-06-03T15:44:44.318691705Z",
                "ends_at": "0001-01-01T00:00:00Z",
                "generator_url": "http://d62d21c44de0:9090/graph?g0.expr=up+%3D%3D+0&g0.tab=1",
            }
        ],
        "receiver": "ESSnotify",
        "status": "firing",
        "version": "4",
        "group_key": '{}:{instance="cadvisor:8080"}',
        "truncated_alerts": 0,
        "group_labels": {"instance": "cadvisor:8080"},
        "common_annotations": {
            "description": "Service cadvisor:8080 is unavailable.",
            "runbook": "http://wiki.alerta.io/RunBook/{app}/Event/{alertname}",
            "value": "DOWN (0)",
        },
        "common_labels": {
            "alertname": "service_down",
            "correlate": "service_up,service_down",
            "dc": "eu-west-1",
            "environment": "Production",
            "instance": "cadvisor:8080",
            "job": "cadvisor",
            "monitor": "codelab",
            "region": "EU",
            "service": "Platform",
            "severity": "major",
        },
        "externalURL": "http://4602c3ba04b6:9093",
    }
    return test1
