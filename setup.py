import setuptools

with open("README.md", "r") as f:
    long_description = f.read()


requirements = [
    "asgiref==3.3.4",
    "certifi==2021.5.30",
    "chardet==4.0.0",
    "click==8.0.1",
    "fastapi==0.65.1",
    "gunicorn==20.1.0",
    "h11==0.12.0",
    "idna==2.10",
    "pydantic==1.8.2",
    "requests==2.25.1",
    "starlette==0.14.2",
    "typing-extensions==3.10.0.0",
    "urllib3==1.26.5",
    "uvicorn==0.14.0",
    "PyYAML==5.4.1",
]
tests_requires = [
    "pytest",
    "pytest-cov",
    "requests-mock",
]

setuptools.setup(
    name="alertmanagert-to-ess-notify",
    description="Bridge to forward alerts to an ESS notify service",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.esss.lu.se/ics-software/ess-notify-server",  # FIXME!!!
    license="BSD-2 license",
    setup_requires=["setuptools_scm"],
    install_requires=requirements,
    packages=setuptools.find_packages(exclude=["tests", "tests.*"]),
    classifiers=[
        "Intended Audience :: Developers",
        "License :: OSI Approved :: BSD License",
        "Operating System :: OS Independent",
        "Programming Language :: Python :: 3 :: Only",
        "Programming Language :: Python :: 3.8",
    ],
    extras_require={"tests": tests_requires},
    python_requires=">=3.8",
)
